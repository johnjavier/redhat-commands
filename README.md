# Redhat Commands

**1. Install Java**

`$ sudo yum install java-1.8.0-openjdk-devel`

`$ java -version`

**2. Install Docker (needed as of writing Docker is not yet on repo of RHEL8)**

Add Repo: `$ sudo curl  https://download.docker.com/linux/centos/docker-ce.repo -o /etc/yum.repos.d/docker-ce.repo`

Make Cache: `$ sudo yum makecache`

Install docker: `$ sudo yum -y  install docker-ce`

Enable docker on server boot: ` $ sudo systemctl enable --now docker `

**3. Sample Docker**
https://dzone.com/articles/run-simple-jar-application-in-docker-container-1

